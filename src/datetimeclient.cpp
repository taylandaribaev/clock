#include "datetimeclient.h"

const int DELAY_TIME = 500;

DateTimeClient::DateTimeClient(QObject *parent)
	: QObject(parent)
{
	connect(&timer_, &QTimer::timeout, this, &DateTimeClient::onTriggered);
	timer_.setInterval(DELAY_TIME);
	timer_.setTimerType(Qt::PreciseTimer);

	currentDateTime_ = QDateTime::currentDateTime();
	timer_.start();
}

QDateTime DateTimeClient::currentDateTime() const
{
	return currentDateTime_;
}

void DateTimeClient::onTriggered()
{
	currentDateTime_ = currentDateTime_.addMSecs(DELAY_TIME);
	emit currentDateTimeChanged();
}
