#ifndef DATETIMECLIENT_H
#define DATETIMECLIENT_H

#include <QObject>
#include <QTimer>
#include <QDateTime>

class DateTimeClient : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QDateTime currentDateTime READ currentDateTime NOTIFY currentDateTimeChanged FINAL)

public:
	explicit DateTimeClient(QObject *parent = nullptr);

	QDateTime currentDateTime() const;

signals:
	void currentDateTimeChanged();

private:
	void onTriggered();

	QTimer timer_;
	QDateTime currentDateTime_;
};

#endif // DATETIMECLIENT_H
