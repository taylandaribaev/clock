import QtQuick 2.12

Item {
    id: root

    signal dateTimeChanged(var dateTime)

    implicitHeight: 480
    implicitWidth: 640

    Connections {
        target: _dateTimeClient

        onCurrentDateTimeChanged: {
            root.dateTimeChanged(_dateTimeClient.currentDateTime);
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            window.stackView.pop()
        }
    }
}
