import QtQuick 2.12
import QtGraphicalEffects 1.0

Item {
    id: root

    property int totalDuration: 900

    property int valueCount: 9
    property int currentValue: 0

    implicitWidth: 50
    implicitHeight: 50

    QtObject {
        id: _private
        property color background: "#ff0000"
        property color activeTextColor: "#f1f2f3"
        property color inactiveTextColor: "#a1a2a3"
        property color roundColor: "#000000"
    }

    Rectangle {
        id: rect

        x: 0
        y: - root.currentValue * width

        Behavior on y {
            NumberAnimation {
                duration: root.totalDuration
                easing.type: Easing.InOutQuad
            }
        }

        radius: 6
        width: root.width
        height: container.height

        color: _private.background

        Column {
            id: container
            width: parent.width

            Repeater {
                model: root.valueCount + 1

                Item {
                    width: container.width
                    height: width

                    Rectangle {
                        id: round

                        anchors.centerIn: parent
                        color: _private.roundColor
                        opacity: root.currentValue == index ? 0.2 : 0

                        width: root.currentValue == index ? parent.width * 1.2 : parent.width * 0.8
                        height: width

                        radius: width / 2

                        Behavior on width {
                            NumberAnimation {
                                duration: root.totalDuration / 2
                            }
                        }

                        Behavior on opacity {
                            NumberAnimation {
                                duration: root.totalDuration / 2
                            }
                        }

                        DropShadow {
                            anchors.fill: round
                            horizontalOffset: 3
                            verticalOffset: 3
                            radius: 6.0
                            samples: 17
                            opacity: round.opacity
                            color: "#80ffffff"
                            source: round
                        }
                    }

                    Text {
                        anchors.centerIn: parent
                        font.pixelSize: 16
                        font.bold: true
                        color: root.currentValue == index ? _private.activeTextColor : _private.inactiveTextColor

                        Behavior on color {
                            ColorAnimation {
                                duration: root.totalDuration / 2
                            }
                        }

                        text: index
                    }
                }
            }
        }
    }

    DropShadow {
        anchors.fill: rect
        horizontalOffset: 3
        verticalOffset: 3
        radius: 8.0
        samples: 17
        color: "#80ffffff"
        source: rect
    }
}
