import QtQuick 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

Clock {
    id: root

    property var currentTime: new Date()
    property bool enableAnimation: true

    onDateTimeChanged: {
        currentTime = dateTime;
    }

    QtObject {
        id: _private
        property color clockColor: "#00000000"
        property color hourNeedleColor: "#000000"
        property color minuteNeedleColor: "#000000"
        property color secondsNeedleColor: "#000000"
    }

    Image {
        anchors.centerIn: parent
        width: 448
        height: 448
        source: "qrc:/analog/ClockAnalogicHousing.png"

        ColorOverlay {
            anchors.fill: parent
            source: parent
            color: _private.clockColor
        }
    }

    Image {
        id: hourHand

        anchors.centerIn: parent
        source: "qrc:/analog/ClockAnalogicHourNeedle.png"
        transform: Rotation {
            origin.x: hourHand.width/2
            origin.y: hourHand.height/2
            angle: {
                if (!isNaN(currentTime.valueOf())) {
                    return (currentTime.getHours() * 30) + (currentTime.getMinutes() * 0.5)
                } else {
                    return 0
                }
            }
            Behavior on angle {
                enabled: enableAnimation
                SpringAnimation { spring: 2; damping: 0.2; modulus: 360 }
            }
        }

        ColorOverlay {
            anchors.fill: parent
            source: parent
            color: _private.hourNeedleColor
        }
    }

    Image {
        id: minuteHand

        anchors.centerIn: parent
        source: "qrc:/analog/ClockAnalogicMinuteNeedle.png"
        transform: Rotation {
            origin.x: minuteHand.width/2
            origin.y: minuteHand.height/2
            angle: {
                if (!isNaN(currentTime.valueOf())) {
                    return currentTime.getMinutes() * 6
                } else {
                    return 5
                }
            }
            Behavior on angle {
                enabled: enableAnimation
                SpringAnimation { spring: 2; damping: 0.2; modulus: 360 }
            }
        }

        ColorOverlay {
            anchors.fill: parent
            source: parent
            color: _private.minuteNeedleColor
        }
    }

    Image {
        id: secondHand

        anchors.centerIn: parent
        source: "qrc:/analog/ClockAnalogicSecondNeedle.png"
        transform: Rotation {
            origin.x: secondHand.width/2
            origin.y: secondHand.height/2
            angle: {
                if (!isNaN(currentTime.valueOf())) {
                    return currentTime.getSeconds() * 6
                } else {
                    return 5
                }
            }
            Behavior on angle {
                enabled: enableAnimation
                SpringAnimation { spring: 1; damping: 0.2; modulus: 360 }
            }
        }

        ColorOverlay {
            anchors.fill: parent
            source: parent
            color: _private.secondsNeedleColor
        }
    }
}
