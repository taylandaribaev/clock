import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12

Window {
    id: window

    property var stackView: _stackView

    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ListModel {
        id: modelClocks

        ListElement {
            name: qsTr("Ticker")
            source: "qrc:/Clock1.qml"
        }

        ListElement {
            name: qsTr("Analog clock")
            source: "qrc:/Clock2.qml"
        }
    }

    Component {
        id: startComponent

        Item {
            ListView {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.topMargin: 50
                anchors.bottomMargin: 50
                anchors.horizontalCenter: parent.horizontalCenter

                width: 300
                model: modelClocks
                spacing: 50

                delegate: RowLayout {
                    width: parent.width
                    height: 30
                    spacing: 10

                    Text {
                        Layout.preferredWidth: 100
                        Layout.minimumWidth: 100

                        text: qsTr("Name:")
                        font.pixelSize: 14
                        font.bold: true
                    }

                    Text {
                        Layout.fillWidth: true

                        text: model.name
                        font.pixelSize: 14
                    }

                    Button {
                        Layout.preferredWidth: 100
                        Layout.minimumWidth: 100

                        text: qsTr("Load")
                        font.pixelSize: 14
                        height: 30
                        width: 100
                        onClicked: {
                            _stackView.push(model.source);
                        }
                    }
                }
            }
        }
    }

    StackView {
        id: _stackView
        anchors.fill: parent
        initialItem: startComponent
    }
}
