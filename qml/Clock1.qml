import QtQuick 2.12

Clock {
    id: root

    onDateTimeChanged: {
        const hour = dateTime.getHours()
        const min = dateTime.getMinutes()
        const sec = dateTime.getSeconds()

        hourHigh.currentValue = Math.trunc(hour / 10)
        hourLow.currentValue = hour % 10

        minHigh.currentValue = Math.trunc(min / 10)
        minLow.currentValue = min % 10

        secHigh.currentValue = Math.trunc(sec / 10)
        secLow.currentValue = sec % 10
    }

    Row {
        anchors.centerIn: parent
        spacing: 30

        Tick {
            id: hourHigh
            valueCount: 2
        }

        Tick {
            id: hourLow
            valueCount: 9
        }

        Item { width: 1; height: 1 }

        Tick {
            id: minHigh
            valueCount: 5
        }

        Tick {
            id: minLow
            valueCount: 9
        }

        Item { width: 1; height: 1 }

        Tick {
            id: secHigh
            valueCount: 5
        }

        Tick {
            id: secLow
            valueCount: 9
        }
    }
}
